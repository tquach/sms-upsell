import json
import logging
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.conf import settings
from reservations.models import Reservation, Customer
from django.http import HttpResponseRedirect, HttpResponse
from datetime import date
from django.views.decorators.csrf import csrf_exempt
from twilio.rest import TwilioRestClient
from twilio import twiml


logger = logging.getLogger(__name__)


def index(request, template_name='reservations/index.html'):
    reservations = Reservation.objects.all()
    return render(request, template_name, {"reservations": reservations})


def create(request, template_name='reservations/create.html'):
    if request.method == 'GET':
        return render(request, template_name)
    else:
        mobile = request.POST.get('mobile')
        if not(mobile):
            mobile = "+14165530280"
        customer = Customer.objects.create(name="John Doe", mobile=mobile, email="tan.quach@gmail.com")
        reservation = Reservation.objects.create(customer=customer, date=date.today(), status="CONFIRMED", details="1 ROOM / QUEEN BED / NONE SMOKING")
        reservations = Reservation.objects.all()
        return render(request, 'reservations/index.html', {"new_reservation": reservation, "reservations": reservations})


def send_sms(request):
    if request.method == 'POST':
        logger.info("Sending SMS ...")

        customer_id = request.POST.get('customer_id')
        logger.info("Customer id %s" % customer_id)

        customer = Customer.objects.get(pk=customer_id)
        if not(customer):
            logger.error("Cannot find customer with id %s" % customer_id)
        else:
            logger.info("Customer %s" % customer.name)
            promo_msg = "Your upcoming reservation has been confirmed. Reply with 'upgrade' at anytime to upgrade to a KING SIZE for FREE."

            client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
            message = client.sms.messages.create(to=customer.mobile, from_=settings.TWILIO_FROM_NUMBER, body=promo_msg)
            logger.info("SMS message sent %s" % message)
            if message.status == 'failed':
                logger.error("Failed to send message")
                return HttpResponse(json.dumps(message.status), mimetype="application/json")

    reservations = Reservation.objects.all()
    return render(request, "reservations/index.html", {"message": message, "reservations": reservations})


@csrf_exempt
def upgrade(request):
    if request.method == 'POST':
        customer_mobile = request.POST.get('From')
        logger.info("Received upgrade request from customer_mobile %s" % customer_mobile)
        customer = Customer.objects.filter(mobile=customer_mobile)
        response = twiml.Response()
        if customer:
            logger.info("Customer found %s" % customer)
            reservation = Reservation.objects.get(customer=customer)
            logger.info("Found reservation %s" % reservation.id)
            body = request.POST.get('Body')

            if body.lower() == 'upgrade':
                if reservation.status == 'UPGRADED':
                    response.sms("You have already upgraded. If you would like to make any other changes, please call our customer service department.")
                else:
                    reservation.status = 'UPGRADED'
                    reservation.details = '1 RM / KING BED / NONE SMOKING'
                    reservation.save()
                    response.sms("Upgrade confirmed. Your reservation has now been upgraded to %s. Thank you and enjoy your stay!" % reservation.details)
            else:
                response.sms("Sorry that response was not valid. No changes were made to your reservation.")
        else:
            response.sms("No customer matching with this mobile number. Please call our customer support line for further assistance.")
        return HttpResponse(response, mimetype="text/xml")
    else:
        return HttpResponseRedirect(reverse("reservations.views.index"))
