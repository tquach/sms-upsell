from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'reservations.views.index'),
    url(r'^create$', 'reservations.views.create'),
    url(r'^send_sms$', 'reservations.views.send_sms'),
    url(r'^upgrade$', 'reservations.views.upgrade'),
)
