from django.db import models


class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField("customer name", max_length=25)
    mobile = models.CharField("customer mobile phone", max_length=40)
    email = models.CharField("customer email", max_length=256)


class Reservation(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateField()
    customer = models.OneToOneField(Customer)
    details = models.CharField("reservation details", max_length=1024)
    status = models.CharField("reservation status", max_length=64)
