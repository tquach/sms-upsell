from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


def homepage(request):
    # just want to redirect to waiting list sign up page
    return HttpResponseRedirect(reverse("reservations.views.index"))


def server_error(request, template_name='core/500.html'):
    """
    A simple 500 handler so we get media
    """
    r = render_to_response(template_name,
        context_instance=RequestContext(request)
    )
    r.status_code = 500
    return r


def server_error_404(request, template_name='core/404.html'):
    """
    A simple 404 handler so we get media
    """
    r = render_to_response(template_name,
        context_instance=RequestContext(request)
    )
    r.status_code = 404
    return r
