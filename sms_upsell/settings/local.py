from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = 'stomh(wcyz4#h7_2(mxe&(69u&$3!xy1vb773e(cxq(9d*39f$6vt'
TWILIO_ACCOUNT_SID = 'AC178771a4c9794f26b022e9e061a21ff6'
TWILIO_AUTH_TOKEN = '3aedb06946c7572760f4830f99f73120'
TWILIO_FROM_NUMBER = '+1 647-931-6813'
#TWILIO_DEFAULT_CALLERID = ''

INSTALLED_APPS += (
    'django_extensions',
    'debug_toolbar',
    #'django_twilio',
)

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': PROJECT_DIR.child('development.db'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}
