from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

import nexus

admin.autodiscover()
nexus.autodiscover()

handler404 = 'core.views.server_error_404'
handler500 = 'core.views.server_error'

urlpatterns = patterns('',
    url(r'^$', 'core.views.homepage', name='homepage'),
    url(r'^reservations/', include('reservations.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^nexus/', include(nexus.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^404/$', handler404),
        (r'^500/$', handler500),
    )
    urlpatterns += staticfiles_urlpatterns()
