from fabric.api import local, env, lcd

env['project_name'] = 'sms_upsell'
env['scss_dir'] = 'scss'
env['static_dir'] = 'static'

def production():
    with open('.epio-app') as file:
        env['epio_app'] = file.read()


def epio(commandstring):
    local('epio {0} -a {1}'.format(
        commandstring,
        env['epio_app']))


def deploy():
    with lcd('%(static_dir)s/%(scss_dir)s' % env):
        local('compass compile --output-style compressed --force')
    with lcd(env['project_name']):
        local('./manage.py collectstatic --ignore %s --noinput' % env['scss_dir'])
        local('./manage.py compress --force')
    epio('upload')
    epio('django syncdb')
    epio('django migrate')
    epio('django epio_flush_cache')
